#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include "command_processing.h"
#include "future_command_table.h"

#define MAX_RECEIVING_DATA_LENGTH 15001
#define MAX_SINGLE_COMMAND_LENGTH 257
#define MAX_LENGTH_OF_DATA_SENT_TO_CLIENT 15001

int command_processing(const char*const series_of_commands_seperated_by_pipe,const unsigned nth_command, future_command_table_type* t) ;

int create_server_socket(int port_that_server_listen_on) ;
void terminated_child_process_collector() ;
void change_working_directory_and_set_PATH() ;
void send_welcome_message(int destination_socket) ;

int main(int argc, char* argv[]){
	int server = create_server_socket(argc==1?11382:atoi(argv[1])) ;

	char buffer[MAX_RECEIVING_DATA_LENGTH] ;
	unsigned number_of_character_read ;

	signal(SIGCHLD,terminated_child_process_collector) ;
	while(1){
		//printf("waiting for a connection...\n") ;
		int connection = accept(server, (struct sockaddr*)NULL, NULL);
		
		pid_t pid = fork() ;
		if(pid==0){

			unsigned command_counter = 0 ;
			// create a table for pipes which store input for future commands.
			future_command_table_type future_command_table ;
			future_command_table_init(&future_command_table) ;

			change_working_directory_and_set_PATH() ;
			
			send_welcome_message(connection) ;
			
			bzero(buffer, MAX_RECEIVING_DATA_LENGTH) ;
			while(strncmp(buffer,"exit",4)!=0){
				write(connection,"% ",strlen("% ")) ;
				bzero(buffer,number_of_character_read) ;
				number_of_character_read = read(connection,buffer,MAX_RECEIVING_DATA_LENGTH) ;
				//write(STDOUT_FILENO,buffer,number_of_character_read) ;
			

				int resultfd ;
				resultfd = command_processing(buffer,command_counter,&future_command_table) ;
				
				command_counter++ ;
				
				// send the content in the resultfd to client
				char result[MAX_LENGTH_OF_DATA_SENT_TO_CLIENT] ;
				bzero(result,MAX_LENGTH_OF_DATA_SENT_TO_CLIENT) ;
				read(resultfd,result,MAX_LENGTH_OF_DATA_SENT_TO_CLIENT) ;
				write(connection,result,strlen(result)) ;
				close(resultfd) ;

			}
			
			close(connection) ;
			exit(0) ;
		}
		else if(pid<0){
			printf("ERROR: fork failed.\n") ;
			exit(1) ;
		}
		//printf("child process: %d\n",pid) ;

		close(connection) ; // *** parent also has to close the socket between client and server! ***
	}

/*
	bzero(buffer,1024) ;
	while(1){
		while(number_of_character_read = read(connection, buffer, 10), number_of_character_read > 0)
		{
			write(STDOUT_FILENO, buffer, number_of_character_read);
			bzero(buffer,number_of_character_read) ;
		}

	}
	close(connection) ;
	*/
	close(server) ;
}


int create_server_socket(int port){
	int server ;
	struct sockaddr_in server_addr ;

	server = socket(AF_INET, SOCK_STREAM, 0) ;
	
	bzero( &server_addr, sizeof(server_addr));
	
	server_addr.sin_family = AF_INET ;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	server_addr.sin_port = htons(port) ;

	bind(server,(struct sockaddr*)&server_addr,sizeof(server_addr)) ;

	listen(server,20) ;

	return server ;
}

void terminated_child_process_collector(){
	int status ;
	pid_t pid = wait3(&status,WNOHANG,(struct rusage*)0) ;
	//printf("pid %d collected.\n",pid) ;
}

void change_working_directory_and_set_PATH(){
	char d[100] = "" ;
	strcat(d,getenv("HOME")) ;
	strcat(d,"/ras") ;
	//printf("working dir: %s.\n",d) ;
	chdir(d) ;

	setenv("PATH","bin:.",1) ;
}

void send_welcome_message(int destination_socket){
	char welcome_message[150] ;
	bzero(welcome_message,150) ;
	strcpy(welcome_message,"****************************************\r\n** Welcome to the information server. **\r\n****************************************\r\n") ;
	write(destination_socket,welcome_message,strlen(welcome_message)) ;
}


