//#include"future_command_table.h"
//int command_processing(const char*const series_of_commands_seperated_by_pipe,future_command_table_type* t) ;
void single_command_execution(const char*const,int*const) ;
char** break_single_command_into_arguments(const char* cmd, int* arg_count) ;
int command_classification(const char*const cmd, const char** pos, int*const len) ;
unsigned mathematic_expression_to_number(char* exp) ;
const char* skip_leading_space(const char*const start) ;
