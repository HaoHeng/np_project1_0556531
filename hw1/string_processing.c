#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"string_processing.h"

#define MAX_STRING_LENGTH 15001

void break_string_into_list(char* str, const char*const deliminators, char** list, int list_len){
	
	/*
	char s[1024] ;
	char* one_word ;
	bzero(s,1024) ;
	strcpy(s,str) ;

	for(one_word = strtok(s,deliminators), *list_len=0; one_word!=NULL ; one_word = strtok(NULL,deliminators), (*list_len)++){
		// do nothing.
	}
	*/
	
	int i ;
	//char s[1024] ; wrong! use strtok(s,deliminators) will result in every elements in list point to a local variable and become dangling pointer!
	//strcpy(s,str) ;
	//char** list = (char**)malloc((list_len+1)*sizeof(char*)) ;
	list[0] = strtok(str,deliminators) ;
	for(i=1 ; i<list_len+1 ; i++){
		list[i] = strtok(NULL,deliminators) ;
	}

}

int number_of_substrings_seperated_by_deliminators(const char*const str, const char*const deliminators){
	char s[MAX_STRING_LENGTH] ;
	char* one_word ;
	bzero(s,MAX_STRING_LENGTH) ;
	strcpy(s,str) ;

	int number_of_substrings ;
	for(one_word = strtok(s,deliminators), number_of_substrings=0; one_word!=NULL ; one_word = strtok(NULL,deliminators), number_of_substrings++){
		// do nothing.
	}

	return number_of_substrings ;

}

/*
// for testing purpose only.
int main(){
	char s[50] = "Hi, this is a small hello.  :))" ;
	char d[10] = " .," ;
	int l = number_of_substrings_seperated_by_deliminators(s,d) ;
	char** list = (char**)malloc((l+1)*sizeof(char*)) ;
	break_string_into_list(s,d,list,l) ;

	int i ;
	printf("list length: %d\n",l) ;
	for(i=0 ; i<l ; i++){
		printf("%s\n",list[i]) ;
	}
	
	free(list) ; // *** don't forget to release the memory ***
}
*/
