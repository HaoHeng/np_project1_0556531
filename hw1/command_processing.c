#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include<fcntl.h>
//#include<errno.h>
#include"command_processing.h"
#include"string_processing.h"
#include"future_command_table.h"

#define MAX_RECEIVING_DATA_LENGTH 15001
#define MAX_SINGLE_COMMAND_LENGTH 257
#define MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND 15001

int command_processing(const char*const c, const unsigned nth_command, future_command_table_type* table){
	char series_of_commands_seperated_by_pipe[MAX_RECEIVING_DATA_LENGTH] ;
	bzero(series_of_commands_seperated_by_pipe,MAX_RECEIVING_DATA_LENGTH) ;
	strcpy(series_of_commands_seperated_by_pipe,c) ;
	//printf("%s",series_of_commands_seperated_by_pipe) ;

	char single_cmd[MAX_SINGLE_COMMAND_LENGTH] ;
	
	/*
	// this will not work because the call to strtok in single_command_execution will result in incorrect result of the strtok function call in this for loop.
	char* single_command ;
	for(single_command = strtok(series_of_commands_seperated_by_pipe,"|") ; single_command!=NULL ; single_command = strtok(NULL,"|")){
		printf("%s\n",single_command) ;
		bzero(single_cmd,1024) ;
		strcpy(single_cmd,single_command) ;
		single_command_execution(single_cmd) ;
		
	}
	*/

	char deliminators[10] = "|" ;
	int number_of_commands_seperated_by_pipe = number_of_substrings_seperated_by_deliminators(series_of_commands_seperated_by_pipe,deliminators) ;
	//printf("number of commands: %d\n", number_of_commands_seperated_by_pipe) ;
	char** command_list = (char**)malloc((number_of_commands_seperated_by_pipe+1)*sizeof(char*)) ;
	break_string_into_list(series_of_commands_seperated_by_pipe,deliminators,command_list,number_of_commands_seperated_by_pipe) ;

	int outpipe[2] ; // pipe which stores stdout.
	int errpipe[2] ; // pipe which stores stderr.
	int stdout_and_stderr_result_fd[2] ; // the result pipe, which is going to be sent to client.
	int pipe_for_future_command[2] ;
	int pipe_for_past_command[2] ;
	if(pipe(outpipe)==-1){
		printf("ERROR: pipe creation fail.\n") ;
	}
	if(pipe(errpipe)==-1){
		printf("ERROR: errpipe creation fail.\n") ;
	}
	if(pipe(stdout_and_stderr_result_fd)==-1){
		printf("ERROR: pipe creationn fail.\n") ;
	}
	if(pipe(pipe_for_past_command)==-1){
		printf("ERROR: pipe creationn fail.\n") ;
	}


	// check input data from past command.
	int there_are_data_from_past_command = 0 ;
	{
		int fd ;
		while(check_future_command_table(table,nth_command,&fd)){
			// copy all data from different pipes into pipe_for_past_command.
			char buf[MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND] ;
			bzero(buf,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;
			read(fd,buf,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;
			write(pipe_for_past_command[1],buf,strlen(buf)) ;

			there_are_data_from_past_command = 1 ;
			close(fd) ;
		}

		close(pipe_for_past_command[1]) ;
	}
	if(!there_are_data_from_past_command) close(pipe_for_past_command[0]) ;

	// save stdin and stdout before redirecting them to pipes.
	int saved_stdin = dup(0) ;
	int saved_stdout = dup(1) ;
	int saved_stderr = dup(2) ;

	int inputfd = there_are_data_from_past_command? pipe_for_past_command[0] : 0 ; // initial inputfd : stdin
	int outputfd = outpipe[1] ; // initial output fd: first pipe.
	int next_inputfd = outpipe[0] ;
	int errfd0 = errpipe[0] ;
	int errfd1 = errpipe[1] ;

	char out_buffer[MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND] ;
	char err_buffer[1024] ;




	int i , cmd_type, command_execution_fail=0 ;
	for(i=0 ; i<number_of_commands_seperated_by_pipe ; i++){

		const char* position ;
		int arglen ;
		cmd_type = command_classification(command_list[i],&position,&arglen) ;
		if(cmd_type==1){ // >
			char filename[100] = "" ;
			strncpy(filename,position,arglen) ;

			char* redirect_symbol = strpbrk(command_list[i],">") ;
			*redirect_symbol = '\0' ;

			FILE* f = fopen(filename,"w") ;
			close(outputfd) ;
			outputfd = fileno(f) ;
		}
		else if(cmd_type==2){ // !5
			
			char number_string[20] ;
			strncpy(number_string,position,arglen) ;

			char* redirect_symbol = strpbrk(command_list[i],"!") ;
			*redirect_symbol = '\0' ;
			
			unsigned delay_for_how_many_commands = atoi(number_string) ;

			//use another pipe to store stdout and stderr.
			if(pipe(pipe_for_future_command)==-1){
				printf("ERROR: pipe creation failed.\n") ;
			}
			
			add_into_future_command_table(table,nth_command+delay_for_how_many_commands,pipe_for_future_command[0]) ;
			

		}
		else if(cmd_type==3){ // |5
			
			char mathematic_expression[200] ;
			strncpy(mathematic_expression,position,arglen) ;

			//char* redirect_symbol = strpbrk(command_list[i],"!") ;
			//*redirect_symbol = '\0' ;

			unsigned number = mathematic_expression_to_number(mathematic_expression) ;
			
			unsigned delay_for_how_many_commands = number ; //atoi(number_string) ;

			//use another pipe to store stdout and stderr.
			if(pipe(pipe_for_future_command)==-1){
				printf("ERROR: pipe creation failed.\n") ;
			}
			
			add_into_future_command_table(table,nth_command+delay_for_how_many_commands,pipe_for_future_command[0]) ;

		}
		else{ // cmd_type==0, normal command.
			// do nothing.
		}
		
		dup2(inputfd,0) ;
		dup2(outputfd,1) ;
		dup2(errfd1,2) ;


		single_command_execution(command_list[i],&command_execution_fail) ;

		// ----- stdin and stdout -----
		if(!(i==0 && !there_are_data_from_past_command)) close(inputfd) ; // when i==0, inputfd is stdin --> do not close stdin!
		close(outputfd) ;
		
		if(pipe(outpipe)==-1){
			printf("ERROR: pipe creation fail.\n") ;
		}
		inputfd = next_inputfd ;
		outputfd = outpipe[1] ;
		next_inputfd = outpipe[0] ;
		// ----------------------------
		
		

		// ---------- stderr ----------
		bzero(err_buffer,1024) ;
		int flags = fcntl(errfd0, F_GETFL, 0);
		fcntl(errfd0, F_SETFL, flags | O_NONBLOCK);
		read(errfd0,err_buffer,1024) ; // the two lines above are to make this read non-blocking.
		if(cmd_type==2){
			write(pipe_for_future_command[1],err_buffer,strlen(err_buffer)) ;
		}
		else{
			write(stdout_and_stderr_result_fd[1],err_buffer,strlen(err_buffer)) ; // immediate write stderr message into the final result pipe, since that '|' does not redirect stderr to next command.
		}
		close(errfd0) ;
		close(errfd1) ;
		if(pipe(errpipe)==-1){
			printf("ERROR: pipe creation fail.\n") ;
		}
		errfd0 = errpipe[0] ;
		errfd1 = errpipe[1] ;
		// ----------------------------


		if(command_execution_fail) break ;

	}

	// transfer stdout result from pipe which stores stdout result into the final result pipe.
	bzero(out_buffer,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;
	int flags = fcntl(inputfd, F_GETFL, 0);
	fcntl(inputfd, F_SETFL, flags | O_NONBLOCK);
	read(inputfd,out_buffer,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;
	if(cmd_type==2 || cmd_type==3){
		write(pipe_for_future_command[1],out_buffer,strlen(out_buffer)) ;	
	}
	else{
		write(stdout_and_stderr_result_fd[1],out_buffer,strlen(out_buffer)) ;
	}

	close(inputfd) ;
	close(stdout_and_stderr_result_fd[1]) ; // will not write anything into final result pipe anymore.
	if(cmd_type==2 || cmd_type==3) close(pipe_for_future_command[1]) ;

	close(outputfd) ;
	close(next_inputfd) ;
	close(errfd0) ;
	close(errfd1) ;

	// restore stdin and stdout.
	dup2(saved_stdin,0) ;
	dup2(saved_stdout,1) ;
	dup2(saved_stderr,2) ;
	close(saved_stdin) ;
	close(saved_stdout) ;
	close(saved_stderr) ;

	free(command_list) ;

	return stdout_and_stderr_result_fd[0] ;

}

void single_command_execution(const char*const cmd, int*const execution_fail){
	
	char deliminators[10] = " \r\n\0" ;
	char c[MAX_SINGLE_COMMAND_LENGTH] ;
	strcpy(c,cmd) ;
	int arguments_count = number_of_substrings_seperated_by_deliminators(cmd,deliminators) ;
	
	char** argument_list = (char**)malloc((arguments_count+1)*sizeof(char*)) ;
	break_string_into_list(c,deliminators,argument_list,arguments_count) ;

	//printf("arg count: %d\n", arguments_count) ;

	
	if(strcmp(argument_list[0],"exit")==0){
		return ;
	}
	else if(strcmp(argument_list[0],"setenv")==0){
		setenv(argument_list[1],argument_list[2],1) ;
		return ;
	}
	
	else if(strcmp(argument_list[0],"printenv")==0){
		//printf("%s\n",getenv(argument_list[1])) ;
		char s[1024] = "" ;
		strcat(s,argument_list[1]) ;
		strcat(s,"=") ;
		strcat(s,getenv(argument_list[1])) ;
		strcat(s,"\r\n") ;
		write(1,s,1024) ;
		return ;
	}
	else if(atoi(argument_list[0])!=0){
		// read data from stdin and trandfer them into stdout directly without doing anything else.
		char buffer[MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND] ;
		bzero(buffer,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;

		int flags = fcntl(0, F_GETFL, 0);
		fcntl(0, F_SETFL, flags | O_NONBLOCK);
		read(0,buffer,MAX_OUTPUT_LENGTH_OF_ONE_SINGLE_COMMAND) ;

		write(1,buffer,strlen(buffer)) ;

		return ;
	}
	

	pid_t pid = fork() ;
	if(pid<0){
		printf("ERROR: fork failed.\n") ;
		exit(1) ;
	}
	else if(pid==0){
		execvp(argument_list[0],argument_list) ;
		
		char s[100] = "" ;
		strcat(s,"Unknown command: [") ;
		strcat(s,argument_list[0]) ;
		strcat(s,"].\r\n") ;
		//printf("Unknow command: [%s].\n",argument_list[0]) ;
		write(2,s,100) ;

		exit(1) ;
	}

	int status ;
	pid_t exec_pid = waitpid(pid,&status,0) ;
	//printf("pid %d process collected.\n",exec_pid) ;

	// actually, status will be 256 times the number that is passed to the exit() as parameter.	
	if(status!=0) *execution_fail = 1 ;


	free(argument_list) ;
}

int command_classification(const char*const cmd, const char** pos, int*const len){
	//char searching_target[20] = ">!123456789" ;
	char searching_target[5] = ">!" ;
	char* target_location ;

	target_location = strpbrk(cmd,searching_target) ;

	if(target_location!=NULL){

		const char* start ;
		const char* end ;
		switch(*target_location){
			case '>':
				start = target_location+1 ;
				start = skip_leading_space(start) ;
				end = strpbrk(start," \n\r\0") ;
				*pos = start ;
				*len = end - start ;
				return 1 ;
				break ;
			case '!':
				// there must be no space between ! and number.  ex: !5
				start = target_location+1 ;
				end = strpbrk(start," \n\r\0") ;
				*pos = start ;
				*len = end - start ;
				return 2 ;
				break ;
			/*
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				return 3 ;
				break ;
			*/
			/*
			target not found, NULL pointer is returned, *target_location result in segmentation fault.
			default:
				printf("normal command.\n") ;
				break ;
			*/
		}

	}
	else{ // act as the default case in the above switch-case statement.
		if(atoi(cmd)!=0){
			const char* start = cmd ;
			const char* end = strpbrk(start," \n\r\0") ;
			*pos = start;
			*len = end - start ;
			return 3 ;
		}
		
		return 0 ;
	}



}

unsigned mathematic_expression_to_number(char* exp){
	char* next_plus_sign ;
	char* current_number_string = exp ;
	next_plus_sign = strpbrk(exp,"+") ;

	unsigned sum = 0 ;
	while(next_plus_sign != NULL){
		sum += atoi(current_number_string) ;
		current_number_string = next_plus_sign+1 ;
		next_plus_sign = strpbrk(current_number_string,"+") ;
	}

	sum += atoi(current_number_string) ;

	return sum ;

}


const char* skip_leading_space(const char*const start){
	
	const char* first_character = start ;
	while(*first_character==' '){
		first_character++ ;
	}
	return first_character ;
}


