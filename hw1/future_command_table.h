typedef
struct future_command_table_type{
	unsigned future_command_number[2048] ;
	unsigned pipe_fd_number[2048] ;
	unsigned* future_pipe_table[2] ; //= {future_command_number,pipe_fd_number} ;

	unsigned size ;
	unsigned next_available_space ;
}
future_command_table_type ;

void future_command_table_init(future_command_table_type* table) ;

void add_into_future_command_table(future_command_table_type* table, const unsigned command_number, const unsigned pipefd_number) ;

int check_future_command_table(future_command_table_type* table, unsigned current_round, unsigned* pipefd_number);

unsigned mod(int a, int b) ;

void print_table(future_command_table_type* table);
